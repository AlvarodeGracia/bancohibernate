package com.alvaro.banco.aplicacion;

public interface OpcionMenu {

	public void ejecutar();

	public String mostrar();
}
