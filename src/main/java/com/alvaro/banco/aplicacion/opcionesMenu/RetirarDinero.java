package com.alvaro.banco.aplicacion.opcionesMenu;

import java.util.Scanner;

import com.alvaro.banco.aplicacion.OpcionMenu;
import com.alvaro.banco.modelo.Cuenta;
import com.alvaro.banco.orm.DAO;
import com.alvaro.banco.orm.DAOFactory;
import com.alvaro.banco.orm.ORM;

public class RetirarDinero implements OpcionMenu {

	@Override
	public void ejecutar() {
		// TODO Auto-generated method stub
		System.out.println("Va a retirar dinero");
		System.out.println("Diga la cuenta");
		String numero =  (new Scanner(System.in)).nextLine();

		DAO<Cuenta, String> dao = (new DAOFactory()).getDao(ORM.orm, Cuenta.class);
		Cuenta cuenta = dao.findByID(numero);
		
		if(cuenta != null) {
			System.out.println("Cuanto dinero quieres retirar?");
			float retirada = (new Scanner(System.in)).nextFloat();
			boolean correcto  =cuenta.retirarDinero(retirada);
			
			if(correcto) {
				System.out.println("Exito, saldo actual"+cuenta.getSaldoCuenta());
				dao.update(cuenta);
				
			}else {
				float maximoRtirada = (-cuenta.getSaldoMin()) + cuenta.getSaldoCuenta();
				System.out.println("Nop uede retirar tanto dinero, el maximo es "+maximoRtirada);
			}

		}else {
			System.out.println("Esa cuenta no existe");
		}
			
	}

	@Override
	public String mostrar() {
		// TODO Auto-generated method stub
		return "Retirar dinero";
	}

}
