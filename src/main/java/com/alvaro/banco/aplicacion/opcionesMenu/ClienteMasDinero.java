package com.alvaro.banco.aplicacion.opcionesMenu;

import java.net.SocketTimeoutException;
import java.util.List;
import java.util.TreeMap;

import com.alvaro.banco.aplicacion.OpcionMenu;
import com.alvaro.banco.modelo.Cliente;
import com.alvaro.banco.modelo.Cuenta;
import com.alvaro.banco.orm.hibernate.Hibernate;

//Si espera un objeto determinado espera ese objeto determinado
//Si esperas un array de esos objetos un list de eso
public class ClienteMasDinero implements OpcionMenu {

	@Override
	public void ejecutar() {
		
		
		String query = "";
		TreeMap<String, String > valores = new TreeMap<String, String >();
		
		//Obtener todos los objetos de esa clase
		query= "FROM Cliente" ;
		
		query = "FROM Cliente as cliente";
		
		query = "FROM Cliente , Cuenta";
		
		query = "FROM Cuenta";
		
		query = "SELECT c.nombre FROM Cliente as c WHERE c.maxAvales < 5";
		
		query = "FROM Contest Ct, Country Cr WHERE Cr.CountryCode = :CountryCode AND Cr.Country IN elements(Ct.RequiredCountries) ";
		
		query = "FROM Cliente cl WHERE cl.nombre = :name";
		
		query = "SELECT cl.nombre, SUM(cuenta.saldoCuenta) FROM Cliente cl JOIN cl.cuentas cuenta GROUP BY cl.nif ORDER BY SUM(cuenta.saldoCuenta) ASC";
		
		query = "SELECT t.company, COUNT(t.cliente) FROM Telefono t GROUP BY t.company";
		
		query = "SELECT em.saldoCuenta FROM CuentaEmpresa em WHERE em.saldoCuenta < 0  ORDER BY em.saldoCuenta ASC";
		
		query = "SELECT cl.nombre, COUNT(cuenta.numeroCuenta) FROM Cliente cl JOIN cl.cuentas cuenta GROUP BY cl.nif";
		
		query = "SELECT c.numeroCuenta, MAX(c.saldoCuenta) FROM Cuenta c";
		
		//valores.put("name", "Alvaro");
		
		System.out.println("Query a ejecutar: "+query);
		//List<Object[]> listObject = Hibernate.getInstance().query(query);
		List<Object[]> listObject = Hibernate.getInstance().prepareQuery(query, valores, 0, 200);
		
		System.out.println("Resultados = "+listObject.size());
		
		System.out.println(listObject.toString());
		
		for(Object[] objects : listObject) {
			for(Object object: objects) {
				
				if (object instanceof Cliente) {
					Cliente cliente = (Cliente) object;
					System.out.println(cliente.toString());
				}else if(object instanceof Cuenta) {
					Cuenta cuenta = (Cuenta) object;
					System.out.println(cuenta.toString());
				}else {
					System.out.println(object.toString());
				}
			}
			
			
		}
		
	}

	@Override
	public String mostrar() {
		// TODO Auto-generated method stub
		return "Ejecutar query";
	}

}
