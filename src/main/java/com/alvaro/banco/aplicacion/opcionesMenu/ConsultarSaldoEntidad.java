package com.alvaro.banco.aplicacion.opcionesMenu;

import java.util.List;

import com.alvaro.banco.aplicacion.OpcionMenu;
import com.alvaro.banco.modelo.Cuenta;
import com.alvaro.banco.orm.DAO;
import com.alvaro.banco.orm.DAOFactory;
import com.alvaro.banco.orm.ORM;



public class ConsultarSaldoEntidad implements OpcionMenu {

	@Override
	public void ejecutar() {

		DAO<Cuenta, String> dao = (new DAOFactory()).getDao(ORM.orm, Cuenta.class);
		List<Cuenta> cuentas = dao.findAll();
		float saldomax = 0;
		for(Cuenta c : cuentas ) {
			System.out.println("Cuenta: "+c.getNumeroCuenta()+" saldo:"+c.getSaldoCuenta());
			saldomax += c.getSaldoCuenta();
		}
		
		System.out.println("Saldo Total Entidad: "+saldomax);
		
		
	}

	@Override
	public String mostrar() {
		// TODO Auto-generated method stub
		return "Consultar Saldo Entidad Financiera";
	}

}
