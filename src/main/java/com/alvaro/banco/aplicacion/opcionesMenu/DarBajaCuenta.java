package com.alvaro.banco.aplicacion.opcionesMenu;

import java.util.Scanner;

import org.hibernate.exception.ConstraintViolationException;

import com.alvaro.banco.aplicacion.OpcionMenu;
import com.alvaro.banco.modelo.Cuenta;
import com.alvaro.banco.orm.DAO;
import com.alvaro.banco.orm.DAOFactory;
import com.alvaro.banco.orm.ORM;

public class DarBajaCuenta implements OpcionMenu{

	@Override
	public void ejecutar() {
		
		System.out.println("Va a dar de baja una cuenta");
		System.out.println("Diga la cuenta");
		String numero =  (new Scanner(System.in)).nextLine();
		
		
		DAO<Cuenta, String> dao = (new DAOFactory()).getDao(ORM.orm, Cuenta.class);
		Cuenta cuenta = dao.findByID(numero);
		
		if(cuenta != null) {
			try {
				
				dao.delete(cuenta);
				
			}catch(ConstraintViolationException e) {
				//!!!!-----!!!!
				System.out.println("No se puede eliminar la cuenta elimine primero a los clientes de esta");
				
			}
			
		}else {
			System.out.println("Esa cuenta no existe");
		}
		
	}

	@Override
	public String mostrar() {
		// TODO Auto-generated method stub
		return "Dar baja Cuenta";
	}

}
