package com.alvaro.banco.aplicacion.opcionesMenu;

import java.util.Scanner;

import com.alvaro.banco.aplicacion.OpcionMenu;
import com.alvaro.banco.modelo.Cuenta;
import com.alvaro.banco.orm.DAO;
import com.alvaro.banco.orm.DAOFactory;
import com.alvaro.banco.orm.ORM;

public class IngresarDinero implements OpcionMenu {

	@Override
	public void ejecutar() {
		System.out.println("Va a ingresar dinero");
		System.out.println("Diga la cuenta");
		String numero =  (new Scanner(System.in)).nextLine();

		DAO<Cuenta, String> dao = (new DAOFactory()).getDao(ORM.orm, Cuenta.class);
		Cuenta cuenta = dao.findByID(numero);
		
		if(cuenta != null) {
			
			System.out.println("Cuanto dinero quieres ingresar?");
			float ingreso = (new Scanner(System.in)).nextFloat();
			cuenta.ingresarDinero(ingreso);
			dao.update(cuenta);
			
		}else {
			System.out.println("Esa cuenta no existe");
		}
		
	}

	@Override
	public String mostrar() {
		// TODO Auto-generated method stub
		return "Ingresar Dinero";
	}

}
