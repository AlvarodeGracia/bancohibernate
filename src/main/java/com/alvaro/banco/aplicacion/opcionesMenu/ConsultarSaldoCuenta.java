package com.alvaro.banco.aplicacion.opcionesMenu;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import com.alvaro.banco.aplicacion.OpcionMenu;
import com.alvaro.banco.modelo.Cuenta;
import com.alvaro.banco.orm.DAO;
import com.alvaro.banco.orm.DAOFactory;
import com.alvaro.banco.orm.ORM;

public class ConsultarSaldoCuenta implements OpcionMenu {

	@Override
	public void ejecutar() {
		System.out.println("Consultando saldo cuenta");
		System.out.println("Diga la cuenta");
		String numero =  (new Scanner(System.in)).nextLine();

		DAO dao = (new DAOFactory()).getDao(ORM.orm, Cuenta.class);
		Cuenta cuenta = (Cuenta) dao.findByID(numero);
		if(cuenta != null) {
			
			System.out.println(cuenta.toString());
			
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			System.out.println("Fecha: "+ dateFormat.format(new Date()));
		}else {
			System.out.println("Esta cuenta n oexiste");
		}
		
		
		
	}

	@Override
	public String mostrar() {
		// TODO Auto-generated method stub
		return "Consultar Saldo Cuenta";
	}

}
