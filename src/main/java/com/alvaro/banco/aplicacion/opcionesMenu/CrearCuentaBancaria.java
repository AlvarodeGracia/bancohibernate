package com.alvaro.banco.aplicacion.opcionesMenu;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

import com.alvaro.banco.aplicacion.OpcionMenu;
import com.alvaro.banco.aplicacion.form.Form;
import com.alvaro.banco.aplicacion.form.Input;
import com.alvaro.banco.aplicacion.form.Input.InputType;
import com.alvaro.banco.modelo.Cliente;
import com.alvaro.banco.modelo.Cuenta;
import com.alvaro.banco.modelo.CuentaEmpresa;
import com.alvaro.banco.modelo.CuentaParticular;
import com.alvaro.banco.modelo.Telefono;
import com.alvaro.banco.orm.DAO;
import com.alvaro.banco.orm.DAOFactory;
import com.alvaro.banco.orm.ORM;

public class CrearCuentaBancaria implements OpcionMenu {

	@Override
	public void ejecutar() {
		System.out.println("Va a crear una cuenta vancaria");
		boolean otroCliente = true;
		Cuenta cuenta = null;
		TreeMap<String, Cliente> clientes = new TreeMap<String, Cliente>();
		do {
			System.out.println("Va a crear un cliente");
			Cliente cliente = new Cliente();
			Form formCliente = new Form(cliente, clientes.keySet());
			formCliente.addInput(new Input("nif", "inserte el nif", InputType.TEXTO));
			formCliente.addInput(new Input("nombre", "inserte Nombre", InputType.TEXTO));
			formCliente.addInput(new Input("maxAvales", "maxx Avalaes", InputType.DECIMAL));
			formCliente.ejecutarForm();
			boolean otroTelefono = false;
			Set<String> telefonos = new HashSet<String>();
			do {

				Telefono telefono = new Telefono();
				Form formTelefono = new Form(telefono, telefonos);
				formTelefono.addInput(new Input("telefono", "Inserte el num de telefono", InputType.TEXTO));
				formTelefono.addInput(new Input("company", "Inserte la compa�ia", InputType.TEXTO));
				formTelefono.ejecutarForm();
				System.out.println("Quiere insertar otro telefono Y");

				telefono = (Telefono) formTelefono.getModelo();
				telefono.setCliente(cliente);
				cliente.getTelefonos().add(telefono);
				telefonos.add(telefono.getTelefono());

				otroTelefono = (new Scanner(System.in)).nextLine().equalsIgnoreCase("Y");

			} while (otroTelefono);

			cliente = (Cliente) formCliente.getModelo();
			clientes.put(cliente.getNif(), cliente);

			System.out.println("�Quieres insertar otro titular? Y");
			otroCliente = (new Scanner(System.in)).nextLine().equalsIgnoreCase("Y");

		} while (otroCliente);

		System.out.println("Ya a insertado los titulares");

		System.out.println("Que tipo de cuenta es:\n 1-Empresa \n 2-Particular");
		int opcion = (new Scanner(System.in)).nextInt();

		switch (opcion) {

		case 1:
			cuenta = new CuentaEmpresa();
			break;
		case 2:
			cuenta = new CuentaParticular();
			break;

		}
		
		//Hay que hacer un casteo a la clase padre para que al realizar
		Form formCuenta = new Form(cuenta);
		formCuenta.setClaseID(Cuenta.class);
		formCuenta.addInput(new Input("numeroCuenta", "Inserte Nuemro de Cuenta", InputType.TEXTO));

		if (cuenta instanceof CuentaEmpresa) {

			formCuenta.addInput(new Input("nombreEmprsa", "Inserte Nombre empresa", InputType.TEXTO));
			formCuenta.addInput(new Input("cifEmpresa", "Inserte Cif Empresa", InputType.TEXTO));
			formCuenta.addInput(new Input("localPropio", "Inserte Local Propio", InputType.LOGICO));

		} else if (cuenta instanceof CuentaParticular) {
			formCuenta.addInput(new Input("isEmitidoTarjeta", "Se ha emitido Tarjeta", InputType.LOGICO));
		}

		formCuenta.ejecutarForm();
		cuenta = (Cuenta) formCuenta.getModelo();

		System.out.println(clientes.values());
		for (Cliente clienteAux : clientes.values()) {
			clienteAux.getCuentas().add(cuenta);
			cuenta.getTitulares().add(clienteAux);
		}

		System.out.println("La cuenta se a creado con " + cuenta.getTitulares().size() + " titulares");

		DAO<Cuenta, String> dao = (new DAOFactory()).getDao(ORM.orm, Cuenta.class);
		dao.insert(cuenta);

		System.out.println("Cuenta Insertada");

	}

	@Override
	public String mostrar() {
		// TODO Auto-generated method stub
		return "Crear Cuenta Bancaria";
	}

}
