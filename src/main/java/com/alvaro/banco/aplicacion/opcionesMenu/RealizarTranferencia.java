package com.alvaro.banco.aplicacion.opcionesMenu;

import java.util.Scanner;

import com.alvaro.banco.aplicacion.OpcionMenu;
import com.alvaro.banco.modelo.Cuenta;
import com.alvaro.banco.orm.DAO;
import com.alvaro.banco.orm.DAOFactory;
import com.alvaro.banco.orm.ORM;

public class RealizarTranferencia implements OpcionMenu{

	@Override
	public void ejecutar() {
		System.out.println("Va a realziar transferencia");
		System.out.println("Diga la cuenta origen");
		String numero =  (new Scanner(System.in)).nextLine();
		DAO<Cuenta, String> dao = (new DAOFactory()).getDao(ORM.orm, Cuenta.class);
		Cuenta cuentaOrigen = dao.findByID(numero);
		if(cuentaOrigen != null) {
			System.out.println("Diga la cuenta Destino");
			numero =  (new Scanner(System.in)).nextLine();
			Cuenta cuentaDestino = dao.findByID(numero);
			if(cuentaDestino != null) {
				
				System.out.println("Cuanto dinero quieres transferir?");
				float transferencia = (new Scanner(System.in)).nextFloat();
				boolean correcto = cuentaOrigen.realizarTransferencia(transferencia);
				float comision = cuentaDestino.calcularComision(transferencia);
				if(correcto) {
					cuentaDestino.ingresarDinero(transferencia);
					System.out.println("Se ha realziado con exito");
					System.out.println("Nuevo saldo origen "+cuentaOrigen.getSaldoCuenta());
					System.out.println("Nuevo saldo dstino "+cuentaDestino.getSaldoCuenta());
					System.out.println("Coste Transferencia "+comision);
					dao.update(cuentaOrigen);
					dao.update(cuentaDestino);
				}else {
					
					System.out.println("No se ha podido transferir");
					float maximoRtirada = (-cuentaOrigen.getSaldoMin()) + cuentaOrigen.getSaldoCuenta() - comision;
					System.out.println("Maximo: "+maximoRtirada);
				
				}
				
				
			}else {
				System.out.println("Esa cuenta n oexiste");
			}
				
		}else {
			System.out.println("Esa cuenta n oexiste");
		}
		
		
	}

	@Override
	public String mostrar() {
		// TODO Auto-generated method stub
		return "Realizar Transferencia";
	}
	

}
