package com.alvaro.banco.aplicacion.opcionesMenu;

import java.util.Scanner;

import com.alvaro.banco.aplicacion.OpcionMenu;
import com.alvaro.banco.modelo.Cliente;
import com.alvaro.banco.modelo.Cuenta;
import com.alvaro.banco.orm.DAO;
import com.alvaro.banco.orm.DAOFactory;
import com.alvaro.banco.orm.ORM;

public class DarBajaCliente implements OpcionMenu{

	@Override
	public void ejecutar() {
		System.out.println("Va a dar de baja un cliente");
		System.out.println("Diga el nif del cliente");
		String numero =  (new Scanner(System.in)).nextLine();
		
		
		DAO<Cliente, String> dao = (new DAOFactory()).getDao(ORM.orm, Cliente.class);
		Cliente cliente = dao.findByID(numero);
		
		if(cliente != null) {
			
			dao.delete(cliente);
			
		}else {
			System.out.println("Este cliente no existe");
		}
		
	}

	@Override
	public String mostrar() {
		// TODO Auto-generated method stub
		return "Dar baja Cliente";
	}

}
