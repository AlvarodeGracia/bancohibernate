package com.alvaro.banco.aplicacion.form;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Set;

import javax.persistence.Column;

import com.alvaro.banco.aplicacion.form.Input.InputType;
import com.alvaro.banco.orm.DAO;
import com.alvaro.banco.orm.DAOFactory;
import com.alvaro.banco.orm.ORM;

//Esta clase es la encargada de validar que los inputs de los form
//han recibido parametros correctos por el usuario para esa variable

public class InputValidator {

	//El tipo de input que es, enum de la clase Input
	private InputType tipo;
	//Lo que el input tiene guardado
	private Object entradaInput;
	//Clase del modelo a comrpobar
	private Class<?> clase;
	//Ids extras donde comprobar si el ID esta ya escogido
	private Set ids;

	//Si este valor es un ID
	private boolean isID;
	// El input no puede estar vacio
	private boolean isNotNull;

	// El texto no puede tener mas de x caratceres
	private boolean isMaxSize;
	private int maxSize;

	// El numero no puede ser mayor de X
	private boolean isMax;
	private int max;

	// El numero no puede ser menor de X
	private boolean isMin;
	private int min;
	
	public InputValidator(Annotation[] annotations, InputType tipo, Object entradaInput) {
		this.tipo = tipo;
		this.entradaInput = entradaInput;
		maxSize = 255;
		for (Annotation annotation : annotations) {

			if (annotation.annotationType().getSimpleName().equals("Id")) {
				//Un id no puede ser null
				isNotNull = true;
				//Para marcar que es un id y comprobar que no este repetido
				isID = true;
			}

			//Si lleva una anotacion column con el valor nullable en false entonces no podra ser null
			if (annotation.annotationType().getSimpleName().equals("Column")) {
				Column column = (Column) annotation;
				if (!column.nullable()) {
					isNotNull = true;
				}
				if(column.length() != 255) {
					maxSize = column.length();
				}
				
			}

		}
	}
	
	public InputValidator(Annotation[] annotations, InputType tipo, Object object,  Set ids) {
		this(annotations, tipo, object);
		this.ids = ids;
	}

	public InputValidator(Annotation[] annotations, InputType tipo, Object object,  Class<?> clase) {
		this(annotations, tipo, object);
		this.clase = clase;
	}
	
	public InputValidator(Annotation[] annotations, InputType tipo, Object object,  Class<?> clase, Set ids) {
		this(annotations, tipo, object);
		this.clase = clase;
		this.ids = ids;
	}

	public boolean comprobar() {

		boolean isCorrecto = true;

		switch (tipo) {

		case TEXTO:
			isCorrecto = comporbarTexto();
			break;
		case DECIMAL:
			break;
		case ENTERO:
			break;
		case FECHA:
			break;
		case LOGICO:
			break;
		default:
			break;

		}

		return isCorrecto;

	}

	private boolean comporbarTexto() {

		boolean isCorrecto = true;

		String texto = (String) entradaInput;
		
		if (isNotNull) {
			if (texto == null) {
				isCorrecto = false;
				System.out.println("Este campo no puede estar vacio");
			} else if (texto.equals("")) {
				isCorrecto = false;
				System.out.println("Este campo no puede estar vacio");
				
			}else if(isID) {
				
				//Si tiene la etiqueta ID significa que es clave primaria lo que conlleva
				//Que este campo no puede ser null, que ya se ha comprobado y que a la vez
				//no esta ya registrado en la BBDD
				
				if(clase != null) {
					//Cojemos el DAO para comprobar que exite el elemento
					
					DAO dao = (new DAOFactory()).getDao(ORM.orm, clase);
					//Lo buscamos
					Object o = dao.findByID(texto);
					System.out.println("Buscar si el id "+texto+" esta en la bbdd");
					//Y vemos si nos ha devuelto algo, si es asi, ponemos el imput como errono para volver a insertar 
					if(o != null) {
						isCorrecto = false;
						System.out.println("Ya esta ese id en la BBDD");
					}
				}
				
				//A veces a parte de comprobar en una BBDD puede que tengamos una lista de IDS
				//Si se la hemos pasado al formulario cuando llegue a esta aprte comprobara que este id
				//no este en esa lista
				if(ids != null) {
					
					for(Object o : ids) {
						
						if(o.equals(texto)) {
							isCorrecto = false;
							System.out.println("Ya ha insertado ese id");
						}
					}
						
					
				}
			} 
		}

		//Comprobamos que tiene el tama�o correcto
		if(texto.length() > maxSize) {
			isCorrecto = false;
			System.out.println("Ha insertado un texto de "+texto.length()+" caracteres");
			System.out.println("El tama�o maximo para insertar es: "+maxSize);
			
		}
		
		return isCorrecto;
	}

}
