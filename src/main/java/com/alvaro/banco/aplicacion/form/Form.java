package com.alvaro.banco.aplicacion.form;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;


//Esta clase representa un Formulario pero para consola, 
//A la vez tiene un apartado de validaciones que se encarga de comprobar 
//si lo que el usuario a insertado cumple los requisitos de los
//modelos de las bbdd para esto se guia de las anotaciones JPA
public class Form {

	@SuppressWarnings("rawtypes")
	//Clase del modelo insertado
	private Class clase;
	//Clase que tiene el id del modelo, normalmente es la misma clase del modelo
	//pero si esta hereda de otra y es el padre el que tiene el ID, debes cambiar esta clase por la del padre
	private Class claseID;
	//El modelo en si
	private Object modelo;

	//Todas las entradas de usuario que tiene el formulario
	private ArrayList<Input> inputs;
	
	//Lista de IDs secudnaria donde comprobar si ya existe,
	private Set<Object> ids;

	//Este constructor es para generar formularios que solo comprueba que un campo ID esta en la BBDD
	public Form(Object object) {

		System.out.println("Creando Formulario");
		this.modelo = object;
		this.clase = object.getClass();
		claseID = clase;
		inputs = new ArrayList<Input>();
		ids = null;
		
	}
	
	//Si quieres que el formulario a parte de comrpobar que esta en la BBDD el id del objeto
		//Puedes pasarle otra lista con los ids que ya estan escogidos.
		public Form(Object object, Set ids) {

			this(object);
			this.ids = ids;
		}

	//Se ejecuta el form, pidiendo por pantalla cada uno de los inputs establecidos
	public void ejecutarForm() {

		for (Input input : inputs) {
			do {

				input.pedirUsuario();

				//Para salir del bucle validar input debe dar true
			} while (!validarInput(input));

		}
		System.out.println("Formulario completado");

	}

	//Agrega un input al formulario
	public void addInput(Input input) {

		inputs.add(input);
	}
	

	//Una vez se ha ejecutado el formulario,se llama a esta funcion para que el modelo actaulice sus
	//valores a los pasados por los input
	public Object getModelo() {

		//Recorremos los inputs
		for (Input input : inputs) {
			//Obtenemos el nombre de la varaible
			String field = input.getVariable();
			//Ponemos la primera letra en mayuscula para que encage con el set
			field = field.substring(0, 1).toUpperCase() + field.substring(1);
			
			Method metodo;
			Class miclass;
			try {
				//instanciamos el metodo set correspondiente
				metodo = modelo.getClass().getMethod("set" + field, input.getEntrada().getClass());
				//Ejecutamos el metodo set
				metodo.invoke(modelo, input.getEntrada());
			} catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return modelo;
	}

	//Este metodo se encarga de validar que l oque tienen los inputs esta correcto
	private boolean validarInput(Input input) {

		boolean isCorrecto = true;
		Field field = null;

		try {
			//Obtenemos el campo de la clase
			field = clase.getDeclaredField(input.getVariable());

		} catch (NoSuchFieldException e) {

			// Si no encuntra ese campo puede ser que sea por que este en la superclase
			// Con esto lo vamos a buscar a la superclase
			try {
				if (clase.getSuperclass() != null)
					field = clase.getSuperclass().getDeclaredField(input.getVariable());

			} catch (NoSuchFieldException e1) {
				System.err.println("El formulario tiene un campo de entrada que n ose corresponde con ninguna varaible del modelo");
				System.err.println("Variable erronea: "+input.getVariable());
			} catch (SecurityException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//Obtenemos todas las anotaciones asignadas a esa varaible
		Annotation[] annotations = field.getDeclaredAnnotations();
		//Creamos un objeto InputValidator dependiendo de si tenemos una segunda lista de donde mirar a parte de en la BBDD
		InputValidator validator;
		if(ids != null) {
			 validator = new InputValidator(annotations, input.getTipo(), input.getEntrada(), claseID, ids);
		}else {
			 validator = new InputValidator(annotations, input.getTipo(), input.getEntrada(), claseID);
		}
		
		isCorrecto = validator.comprobar();

		return isCorrecto;

	}

	//Si queremos cambiar la clase
	public void setClaseID(Class clase) {
		this.claseID = clase;
	}

	
	
}
