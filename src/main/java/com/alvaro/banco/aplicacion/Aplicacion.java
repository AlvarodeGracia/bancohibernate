package com.alvaro.banco.aplicacion;

import com.alvaro.banco.orm.DAOFactory;
import com.alvaro.banco.orm.hibernate.Hibernate;

public class Aplicacion {

	public static void main(String[] args) {

		System.out.println(".......Aplicacion Carrera......\n");

		AppMenu appMenu = new AppMenu();
		//Iniciamos Hibernate
		Hibernate.getInstance();
		appMenu.run();

	}

}
