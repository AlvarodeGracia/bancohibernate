package com.alvaro.banco.orm;

import java.io.Serializable;

import com.alvaro.banco.orm.hibernate.DAOHibernate;

public class DAOFactory<T, K extends Serializable> {

	public static final int HIBERNATE = 1;

	public DAO<T, K> getDao(int tipo, Class<T> type) {

		DAO<T, K> dao = null;
		switch (tipo) {
		case 1:
			dao = new DAOHibernate<T, K>(type);
			break;
		case 2:
			break;

		}

		return dao;

	}

}
