package com.alvaro.banco.modelo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

@Entity
@Table(name = "Clientes")
public class Cliente  implements Serializable{

	@Id
	private String nif;
	private String nombre;
	private float maxAvales;

	@OneToMany(mappedBy = "cliente",cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.EAGER)
	private Set<Telefono> telefonos;
	
	@ManyToMany(mappedBy="titulares")
	private Set<Cuenta> cuentas;

	public Cliente() {
		super();
		this.nif = "";
		this.nombre = "";
		this.maxAvales = 0;
		this.telefonos = new HashSet();
		this.cuentas = new HashSet();
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getMaxAvales() {
		return maxAvales;
	}

	public void setMaxAvales(Float maxAvales) {
		this.maxAvales = maxAvales;
	}

	public Set<Telefono> getTelefonos() {
		return telefonos;
	}

	public void setTelefonos(Set<Telefono> telefonos) {
		this.telefonos = telefonos;
	}
	

	public Set<Cuenta> getCuentas() {
		return cuentas;
	}

	public void setCuentas(Set<Cuenta> cuentas) {
		this.cuentas = cuentas;
	}

	public void setMaxAvales(float maxAvales) {
		this.maxAvales = maxAvales;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nif == null) ? 0 : nif.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (nif == null) {
			if (other.nif != null)
				return false;
		} else if (!nif.equals(other.nif))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Cliente [nif=" + nif + ", nombre=" + nombre + ", maxAvales=" + maxAvales + ", telefonos=" + telefonos
				+ "]";
	}

}
