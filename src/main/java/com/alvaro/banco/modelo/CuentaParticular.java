package com.alvaro.banco.modelo;

import java.io.Serializable;

import javax.persistence.Entity;

@Entity
public class CuentaParticular extends Cuenta implements Serializable{
	
	boolean isEmitidoTarjeta;

	public CuentaParticular() {
		super();
		this.isEmitidoTarjeta = false;
	}

	public boolean isEmitidoTarjeta() {
		return isEmitidoTarjeta;
	}

	public void setIsEmitidoTarjeta(Boolean isEmitidoTarjeta) {
		this.isEmitidoTarjeta = isEmitidoTarjeta;
	}
	
	@Override
	public String toString() {
		return "CuentaParticular [isEmitidoTarjeta=" + isEmitidoTarjeta + ", toString()=" + super.toString() + "]";
	}

	@Override
	public float getSaldoMin() {
		float sum = 0;
		for(Cliente cliente: super.getTitulares()) {
			sum += cliente.getMaxAvales();
		}
		
		float saldoMin = -(sum/2);
		
		return saldoMin;
	}

	@Override
	public float calcularComision(float cantidad) {
		
		float comision = cantidad * 0.2f / 100; 
		int max = 4;
		comision = (comision > max)? max : comision;
	
		return comision;
	}
	
	

}
