package com.alvaro.banco.modelo;

import java.io.Serializable;

import javax.persistence.Entity;

@Entity
public class CuentaEmpresa extends Cuenta implements Serializable{
	
	public enum Local{
		PROPIO, ALQUILADO
	}
	
	private String nombreEmprsa;
	private String cifEmpresa;
	private boolean localPropio;
	
	public CuentaEmpresa() {
		super();
		this.nombreEmprsa = "";
		this.cifEmpresa = "";
		this.localPropio = false;
	}

	public String getNombreEmprsa() {
		return nombreEmprsa;
	}

	public void setNombreEmprsa(String nombreEmprsa) {
		this.nombreEmprsa = nombreEmprsa;
	}

	public String getCifEmpresa() {
		return cifEmpresa;
	}

	public void setCifEmpresa(String cifEmpresa) {
		this.cifEmpresa = cifEmpresa;
	}

	public boolean isLocalPropio() {
		return localPropio;
	}

	public void setLocalPropio(Boolean localPropio) {
		this.localPropio = localPropio;
	}

	@Override
	public String toString() {
		return "Cuenta: "+super.toString()+" -CuentaEmpresa [nombreEmprsa=" + nombreEmprsa + ", cifEmpresa=" + cifEmpresa + ", localPropio="
				+ localPropio + "]";
	}

	@Override
	public float getSaldoMin() {
		float sum = 0;
		for(Cliente cliente: super.getTitulares()) {
			sum += cliente.getMaxAvales();
		}
		
		float saldoMin = -(sum*2);
		
		return saldoMin;
	}

	@Override
	public float calcularComision(float cantidad) {
		float comision = cantidad * 0.1f / 100;
		int max = 6;
		comision = (comision > max)? max : comision;
	
		return comision;
	}

	
	
	
	
	

}
