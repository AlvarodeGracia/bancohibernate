package com.alvaro.banco.modelo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Cuenta implements Serializable{

	@Id
	private String numeroCuenta;
		
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE}, fetch = FetchType.EAGER)
		//@JoinTable(name="ClienteCuenta", joinColumns={@JoinColumn(name="numeroCuenta")}, inverseJoinColumns={@JoinColumn(name="nif")})
	private Set<Cliente> titulares;
	
	private float saldoCuenta;
	
	public Cuenta() {
		super();
		this.numeroCuenta = "";
		this.titulares = new HashSet();
		this.saldoCuenta = 0;
	}
	
	public void ingresarDinero(float cantidad) {
		saldoCuenta += cantidad;
	}
	
	public abstract float getSaldoMin();
	
	public boolean retirarDinero(float cantidad) {
		
		boolean isRetirado = false;
		float auxSaldo = saldoCuenta - cantidad;
		
		if(auxSaldo > getSaldoMin()) {
			
			saldoCuenta -= cantidad;
			isRetirado = true;
		}
		
		return isRetirado;
		
	}
	
	public abstract float calcularComision(float cantidad);
	
	public boolean realizarTransferencia(float cantidad) {
		
		float comision = calcularComision(cantidad);
		boolean correcto = retirarDinero(cantidad+comision);
		
		return correcto;
	}
	
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public Set<Cliente> getTitulares() {
		return titulares;
	}
	public void setTitulares(Set<Cliente> titulares) {
		this.titulares = titulares;
	}
	public float getSaldoCuenta() {
		return saldoCuenta;
	}
	public void setSaldoCuenta(float saldoCuenta) {
		this.saldoCuenta = saldoCuenta;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numeroCuenta == null) ? 0 : numeroCuenta.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cuenta other = (Cuenta) obj;
		if (numeroCuenta == null) {
			if (other.numeroCuenta != null)
				return false;
		} else if (!numeroCuenta.equals(other.numeroCuenta))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Cuenta [numeroCuenta=" + numeroCuenta + ", titulares=" + titulares + ", saldoCuenta=" + saldoCuenta
				+ "]";
	}
	
	
	
	
}
